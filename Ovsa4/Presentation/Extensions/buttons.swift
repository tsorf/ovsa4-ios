import Foundation
import UIKit

public extension UIButton {

    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {

        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        setBackgroundImage(colorImage, for: state)
    }
}

open class BaseButton : UIButton {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareView()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    open func prepareView() {
        
    }
}

open class BaseLabel : UILabel {
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareView()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    open func prepareView() {
        
    }
}
