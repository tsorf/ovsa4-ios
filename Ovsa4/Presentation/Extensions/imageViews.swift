import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    public func loadImage(_ url: String?, placeHolder: UIImage?) {
        guard let urlString = url else {
            return
        }
        self.image = nil
        if urlString.starts(with: "data") {
            loadImageFromBase64(withData: urlString)
            return
        } else {
            if let cachedImage = imageCache.object(forKey: NSString(string: urlString)) {
                self.image = cachedImage
                return
            }
            
            if let url = URL(string: urlString) {
                loadImage(url: url, key: urlString, placeholder: placeHolder)
            }
        }
    }
    
    private func loadImage(url: URL, key: String?, placeholder: UIImage? = nil) {
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            
            //print("RESPONSE FROM API: \(response)")
            if error != nil {
                DispatchQueue.main.async {
                    self.image = placeholder
                }
                return
            }
            DispatchQueue.main.async {
                if let data = data {
                    if let downloadedImage = UIImage(data: data) {
                        if let key = key {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: key))

                        }
                        self.image = downloadedImage
                    }
                }
            }
        }).resume()
    }
    
    private func loadImageFromBase64(withData data: String) {
        let string = data.replacingOccurrences(of: "\r\n", with: "")
        if let url = URL(string: string) {
            loadImage(url: url, key: nil)
        }
//        let dataDecoded: Data = Data(base64Encoded: string, options: .ignoreUnknownCharacters)!
//        let decodedimage = UIImage(data: dataDecoded)
//        self.image = decodedimage
    }
}
