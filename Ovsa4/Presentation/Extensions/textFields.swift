import Foundation
import UIKit

public extension UITextField {
    
    func trimmedOrNull() -> String? {
        guard let value = text?.trim() else {
            return nil
        }
        if value.isEmpty {
            return nil
        }
        return value
    }
    
    func isTrimmedEmpty() -> Bool {
        guard let text = text else {
            return false
        }
        return text.trim().isEmpty
    }
}

@objc open class BaseTextField : UITextField {
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareView()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    open func prepareView() {
        
    }
}

@objc open class ObservableTextField: BaseTextField {
    
    public var observer: ((Bool) -> Void)?
    
    open override func prepareView() {
        addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        observer?(text != nil && !text!.trim().isEmpty)
    }
}
