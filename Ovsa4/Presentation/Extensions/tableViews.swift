import Foundation
import UIKit

public extension UITableView {
    
    func disableEmptyLine() {
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
}
