import Foundation

public protocol BasicViewProtocol {
    func enableControls(enabled: Bool, code: Int)
    func showMessage(message: String?, code: Int)
}

public protocol ViewHolder {
    func provideView() -> BasicViewProtocol
}

public protocol ModelViewProtocol {
    associatedtype Model
    
    var model: Model? { get set}
}

public protocol InteractiveModelViewProtocol : ModelViewProtocol {
    associatedtype Model
    
    var listener: InteractiveModelViewListenerProtocol {get set}
    
    
}

public protocol InteractiveModelViewListenerProtocol {
    func onModelAction(code: Int, payload: Any?)
}
