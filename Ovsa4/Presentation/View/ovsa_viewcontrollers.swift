import Foundation
import UIKit

public extension UIViewController {
    
    convenience init(payload: Codable?, withParams params: [Codable] = []) {
        self.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    convenience init(withCommonNib nib: String) {
        self.init(nibName: nib, bundle: nil)
    }
}

open class PresenterViewController: UIViewController, BasicViewProtocol {
    
    private var rightButtonListener: (() -> Void)?
    private var rightButtonAsDropdown = false
    private var alert: UIAlertController?
    
    open func providePresenter() throws -> LivePresenterProtocol? {
        throw RuntimeError(message: "presenter is not provided")
    }
    
    open func initView() {
        
    }
    
    open func configurePresenter() {
        
    }
    
    
    public func blockScreen(blocked: Bool, _ message: String? = nil) {
        if blocked {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();

            alert.view.addSubview(loadingIndicator)
            
            self.alert?.dismiss(animated: true, completion: nil)
            present(alert, animated: true, completion: nil)
            self.alert = alert
        } else {
            self.alert?.dismiss(animated: true, completion: nil)
        }
    }
    
    open func addRightButton(title: String, asDropdown: Bool = false, listener: @escaping () -> Void) {
        self.rightButtonListener = listener
        self.rightButtonAsDropdown = asDropdown
        let button = UIBarButtonItem(title: prepareRightButtonTitle(original: title), style: .plain, target: self, action: #selector(onRightButtonTap))
        navigationItem.rightBarButtonItem = button
    }
    
    open func addRightButton(image: UIImage, listener: @escaping () -> Void) {
        self.rightButtonListener = listener
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(onRightButtonTap))
        navigationItem.rightBarButtonItem = button
    }
    
    public func setRightButtonTitle(_ title: String, asDropdown: Bool) {
        guard let button = navigationItem.rightBarButtonItem else { return }
        self.rightButtonAsDropdown = asDropdown
        let preparedTitle = prepareRightButtonTitle(original: title)
        button.title = preparedTitle
    }
    
    private func prepareRightButtonTitle(original: String) -> String {
        return rightButtonAsDropdown ? "\(original) ▼" : original
    }
    
    @objc
    func onRightButtonTap() {
        rightButtonListener?()
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        self.configurePresenter()
        try? providePresenter()?.viewDidLoad()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        try? providePresenter()?.viewDidAppear()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        try? providePresenter()?.viewDidDisappear()
    }
    
    open func enableControls(enabled: Bool, code: Int) {
    }
    
     
    public func registerTextChangeListeners(_ listener: @escaping(ObservableTextField, Bool) -> Void, items: ObservableTextField...) {
        for item in items {
            item.observer = { selected in
                listener(item, selected)
            }
        }
    }
    
    

}

public extension UIViewController {
    
    func showMessage(message: String?) {
        showMessage(message: message, code: 0)
    }
    
    
    
    func openUrl(url: String) {
        guard let url = URL(string: url) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func showMessage(message: String?, code: Int) {
        self.showMessage(message: message, code: code, completion: {})
    }
    
    func showMessage(message: String?, code: Int, completion: @escaping () -> Void = {}) {
        var title: String = ""
        switch code {
        case -1:
            title = "Ошибка"
        case 1:
            title = "Успешно"
        default:
            title = "Сообщение"
        }
        let alert = UIAlertController(title: title, message: message ?? "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            completion()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showMessage(title: String, message: String, completion: @escaping () -> Void = {}) {
        let alert = UIAlertController(title: title, message: message ?? "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            completion()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    func callPhone(phone: String) {
        guard let url = URL(string: "tel://" + phone) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func show(_ controller: UIViewController) {
        controller.modalPresentationStyle = .fullScreen
        show(controller, sender: self)
    }
}
