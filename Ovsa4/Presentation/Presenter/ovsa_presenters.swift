import Foundation
import RxSwift

public protocol LivePresenterProtocol {
//    associatedtype View
    
    func onAttach(holder: Any, params: Any?)
    
    func viewDidLoad()
    
    func viewDidAppear()
    
    func viewDidDisappear()
}

public protocol DisposableContainerProtocol {
    var disposable: CompositeDisposable { get }
}

precedencegroup DispoablePrecedence {
    associativity: left
}

infix operator ++ : DispoablePrecedence

public func ++ (left: DisposableContainerProtocol, right: Disposable) {
    _ = left.disposable.insert(right)
}

open class BasePresenter<T> : LivePresenterProtocol, DisposableContainerProtocol {
    
    
    
    public typealias View = T
    public var holder: T!
    public let disposable = CompositeDisposable()
    
    public init() {
        
    }
    
    
    public func enableControls(_ enabled: Bool, code: Int = 0) {
        if let holder = holder as? ViewHolder {
            holder.provideView().enableControls(enabled: enabled, code: code)
        }
    }
    
    public func showSuccess(_ message: String) {
        if let holder = holder as? ViewHolder {
            holder.provideView().showMessage(message: message, code: 1)
        }
    }
    
    public func showError(_ message: String) {
        if let holder = holder as? ViewHolder {
            holder.provideView().showMessage(message: message, code: -1)
        }
    }
    
    open func onAttach(holder: Any, params: Any?) {
        guard let h = holder as? View else {
            print("Wrong view type: \(holder)")
            return
        }
        self.holder = h
    }
    
    open func viewDidLoad() {
        
    }
    
    open func viewDidAppear() {
        
    }
    
    open func viewDidDisappear() {
//        disposable.dispose()
    }
}
