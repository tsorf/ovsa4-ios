import Foundation

public protocol Configurator {
    associatedtype T
    func configure(_ entity: T)
}
