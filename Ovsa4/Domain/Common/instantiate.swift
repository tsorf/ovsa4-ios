import Foundation
import UIKit

public protocol Initializable {
    init()
}

extension UIViewController : Initializable {
    
}

public func instance<T>(typeThing:T.Type) -> T where T:Initializable {
    return typeThing.init()
}


