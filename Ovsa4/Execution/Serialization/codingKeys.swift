import Foundation

struct GenericCodingKeys: CodingKey {
    var stringValue: String
    var intValue: Int?
    
    init?(stringValue: String) { self.stringValue = stringValue }
    init?(intValue: Int) { self.intValue = intValue; self.stringValue = "\(intValue)" }
    static func makeKey(_ stringValue: String) -> GenericCodingKeys { return self.init(stringValue: stringValue)! }
    static func makeKey(_ intValue: Int) -> GenericCodingKeys { return self.init(intValue: intValue)! }
}

/// A structure that retains just the decoder object so we can decode dynamically later
fileprivate struct JSONHelper: Decodable {
    let decoder: Decoder
    
    init(from decoder: Decoder) throws {
        self.decoder = decoder
    }
}

public func deserialize<T: Decodable>(from json: Data, field: String) throws -> T {
    let helper = try JSONDecoder().decode(JSONHelper.self, from: json)
    let container = try helper.decoder.container(keyedBy: GenericCodingKeys.self)
    
    return try container.decode(T.self, forKey: .makeKey(field))
}

public func deserialize<T: Decodable>(from json: Data) throws -> T {
    return try JSONDecoder().decode(T.self, from: json)
}

public func serialize<T : Encodable>(_ model: T) throws -> String {
    let data = try JSONEncoder().encode(model)
    if let result = String(data: data, encoding: .utf8) {
        return result
    }
    throw RuntimeError(message: "failed to serialize \(model)")
}
