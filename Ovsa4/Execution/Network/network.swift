import Foundation
import Alamofire
import RxSwift
import Alamofire_Synchronous


public struct JSON {
    static let encoder = JSONEncoder()
}

public extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
}

public extension Observable {
    
    func consume(_ success: @escaping(Element) -> Void, _ failure: @escaping(Error) -> Void) -> Disposable {
        return self
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (element) in
            success(element)
        }, onError: { (error) in
            failure(error)
        }, onCompleted: {
            
        }) {
            
        }
    }
    
    func toFront() -> Observable<Element> {
        return self.subscribeOn(MainScheduler.instance)
    }
    
    func toBack() -> Observable<Element> {
        return self.observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    }
}

extension Single {
    
    @discardableResult
    public func consume(_ success: @escaping(Element) -> Void, _ failure: @escaping(Error) -> Void) -> Disposable {
        return self
            .asObservable()
            .consume(success, failure)
        
    }
    
    public func toFront() -> PrimitiveSequence<Trait, Element> {
        return self.subscribeOn(MainScheduler.instance)
    }
    
    public func toBack() -> PrimitiveSequence<Trait, Element> {
        return self.observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    }
}

public protocol IdentityProviderProtocol {
    func provideIdentity() -> String?
}

open class ApiClient {
    
    private let baseUrl: String
    
    public init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    open func identityProvider() -> IdentityProviderProtocol? {
        return nil
    }
    
    public func apiUrl(route: String) -> String {
//        let url = Bundle.main
        return baseUrl + route
    }
    

    @discardableResult
    public func call<T : Codable>(_ method: HTTPMethod, _ route: String, _ type: T.Type, _ request: Encodable? = nil) -> Observable<T> {
        var headers = HTTPHeaders()
        if let token = identityProvider()?.provideIdentity() {
            headers["Authorization"] = "Bearer " + token
        }
        
        let encoding: ParameterEncoding
        if method == .get {
            encoding = URLEncoding.queryString
        } else {
            encoding = JSONEncoding.default
        }
        return Observable.just(true).toBack()
            .map { _ in
                let response = Alamofire.SessionManager.default.request(self.apiUrl(route: route), method: method, parameters: request?.dictionary, encoding: encoding, headers: headers).response()
                if let error = response.error {
                    throw ApiError(response.response?.statusCode ?? 0, error.localizedDescription)
                } else {
                    if let data = response.data,
                        let response = response.response {
                        switch response.statusCode {
                        case 200...299:
                            do {
                                let result: T = try deserialize(from: data)
                                return result
                            } catch {
                                throw ApiError(response.statusCode, "deserialization error")
                            }
                        default:
                            guard let message: MessageResponse = try? deserialize(from: data) else {
                                throw ApiError(response.statusCode, "error body deserialization error")
                            }
                            throw ApiError(response.statusCode, message.message)
                        }
                    } else {
                        throw ApiError(response.response?.statusCode ?? 0, "no response data")
                    }
                }
                
        }
        
//        return data(method, apiUrl(route: route), parameters: request?.dictionary, encoding: URLEncoding.default, headers: headers).debug().map { (data) -> T in
//            let identity: T = try! deserialize(from: data)
//            return identity
//        }.do(onError: { (error) in
//            print(error)
//            if case Alamofire.AFError.ResponseValidationFailureReason.unacceptableStatusCode(code: 400) = error {
//
//            }
//        })
        
    }
}
