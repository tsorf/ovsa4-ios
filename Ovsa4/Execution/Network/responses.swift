import Foundation


public struct YiiArrayResponse<T: Codable>: Codable {
    public let items: [T]
}

public struct MessageResponse: Codable {
    public let message: String
    public let code: Int = 0
}
