import Foundation

public class RuntimeError: NSObject, LocalizedError {
    let message: String
    
    public init(message: String) {
        self.message = message
    }
    
    public override var description: String {
        get {
            return message
        }
    }
    public var errorDescription: String? {
        get {
            return self.description
        }
    }
}

public class ApiError: NSObject, LocalizedError {
    public let code: Int
    public let message: String?
    
    public init(_ code: Int, _ message: String? = nil) {
        self.message = message
        self.code = code
    }
    
    public override var description: String {
        get {
            return message ?? String(code)
        }
    }
    public var errorDescription: String? {
        get {
            return self.description
        }
    }
}
