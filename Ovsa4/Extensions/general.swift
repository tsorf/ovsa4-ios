import Foundation

public protocol HasApply { }

public extension HasApply {
    
    @discardableResult
    func apply(closure:(Self) -> ()) -> Self {
        closure(self)
        return self
    }
}

extension NSObject: HasApply { }
