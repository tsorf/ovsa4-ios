import UIKit

public protocol Repository {
    func load<Model : Decodable>() -> Model?
    func save(model: Encodable?)
}

open class BaseRepo<T : Codable> : Repository {
    public func load<Model>() -> Model? where Model : Decodable {
        if Model.self == T.self {
            if self.model == nil {
                let modelName = name()
                if let data = storage.string(forKey: modelName) {
                    model = try? deserialize(from: data.data(using: .utf8)!)
                } else {
                    print("no local copy for \(modelName)")
                }
            }
            if self.model == nil {
                model = handleNull()
            }
            return model as? Model
        } else {
            print("wrong type \(Model.self) for repo \(self)")
            return nil
        }
    }
    
    open func save(model: Encodable?) {
        if let model = model as? T? {
            self.model = model
            if model == nil {
                storage.set(nil, forKey: name())
            } else {
                try! storage.set(serialize(model: model), forKey: name())
            }
        } else {
            print("wrong type \(model.debugDescription) for repo \(self)")
        }
    }
    
    
    private var model: T? = nil
    private let storage: UserDefaults = UserDefaults.standard
    
    public init() {
        
    }
    
    open func handleNull() -> T? {
        return nil
    }
    
    public func executeAndSave(block: (T) -> T) {
        if let existed = model {
            let update = block(existed)
            try! self.save(model: update)
        }
    }
    
    
    public func name() -> String {
        return String(describing: T.self)
    }
    
    private func serialize(model: T?) throws -> String? {
        if model == nil {
            return nil
        } else {
            if let encodedData = try? JSONEncoder().encode(model) {
                return String(data: encodedData, encoding: String.Encoding.utf8)
            } else {
                throw RuntimeError(message: "Serialization error")
            }
        }
    }
}
